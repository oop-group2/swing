/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Natthakritta
 */
public class Frame1 {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        //frame.setSize(new Dimension(500,300));

        JLabel lblHelloWorld = new JLabel("Hello World!", JLabel.CENTER);
        frame.add(lblHelloWorld);
        lblHelloWorld.setBackground(Color.orange);
        lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("Vendana",Font.PLAIN,18));
        
        frame.setVisible(true);//set 

    }
}
